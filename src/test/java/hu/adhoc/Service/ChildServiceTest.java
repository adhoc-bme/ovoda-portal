package hu.adhoc.Service;

import hu.adhoc.Dao.ChildDao;
import hu.adhoc.Entity.Child;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class ChildServiceTest {

    private ChildService childServiceUnderTest;

    @BeforeEach
    public void setUp() {
        childServiceUnderTest = new ChildService();
        childServiceUnderTest.childDao = mock(ChildDao.class);
    }

    @Test
    public void testFindAllChild() {
        // Setup
        final Iterable<Child> expectedResult = null;

        // Run the test
        final Iterable<Child> result = childServiceUnderTest.findAllChild();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testAddChild() {
        // Setup
        final Child child = null;

        // Run the test
        childServiceUnderTest.addChild(child);

        // Verify the results
    }

    @Test
    public void testFindById() {
        // Setup
        final Integer id = 0;
        final Optional<Child> expectedResult = null;

        // Run the test
        final Optional<Child> result = childServiceUnderTest.findById(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testDeleteChild() {
        // Setup
        final Integer id = 0;

        // Run the test
        childServiceUnderTest.deleteChild(id);

        // Verify the results
    }
}
