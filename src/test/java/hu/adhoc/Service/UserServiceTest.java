package hu.adhoc.Service;

import hu.adhoc.Dao.AddressDao;
import hu.adhoc.Dao.AlertsDao;
import hu.adhoc.Dao.EventsDao;
import hu.adhoc.Dao.UserDao;
import hu.adhoc.Entity.Address;
import hu.adhoc.Entity.Alerts;
import hu.adhoc.Entity.Events;
import hu.adhoc.Entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class UserServiceTest {

    private UserService userServiceUnderTest;

    @BeforeEach
    public void setUp() {
        userServiceUnderTest = new UserService();
        userServiceUnderTest.userDao = mock(UserDao.class);
        userServiceUnderTest.eventsDao = mock(EventsDao.class);
        userServiceUnderTest.alertsDao = mock(AlertsDao.class);
        userServiceUnderTest.addressDao = mock(AddressDao.class);
    }

    @Test
    public void testGetAllParents() {
        // Setup
        final Iterable<User> expectedResult = null;

        // Run the test
        final Iterable<User> result = userServiceUnderTest.getAllParents();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAllTeachers() {
        // Setup
        final Iterable<User> expectedResult = null;

        // Run the test
        final Iterable<User> result = userServiceUnderTest.getAllTeachers();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetUserById() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = userServiceUnderTest.getUserById(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testDeleteUser() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = userServiceUnderTest.deleteUser(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testUpdateUser() {
        // Setup
        final User user = null;

        // Run the test
        userServiceUnderTest.updateUser(user);

        // Verify the results
    }

    @Test
    public void testAddUser() {
        // Setup
        final User user = null;

        // Run the test
        userServiceUnderTest.addUser(user);

        // Verify the results
    }

    @Test
    public void testLogin() {
        // Setup
        final String name = "name";
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = userServiceUnderTest.login(name);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testLogout() {
        // Setup
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = userServiceUnderTest.logout();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAllUsers() {
        // Setup
        final Iterable<User> expectedResult = null;

        // Run the test
        final Iterable<User> result = userServiceUnderTest.getAllUsers();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetMicimacko() {
        // Setup
        final Iterable<User> expectedResult = null;

        // Run the test
        final Iterable<User> result = userServiceUnderTest.getMicimacko();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testPostEvent() {
        // Setup
        final Events event = null;

        // Run the test
        userServiceUnderTest.postEvent(event);

        // Verify the results
    }

    @Test
    public void testDeleteEventById() {
        // Setup
        final Integer id = 0;

        // Run the test
        userServiceUnderTest.deleteEventById(id);

        // Verify the results
    }

    @Test
    public void testPostAlert() {
        // Setup
        final Alerts alerts = null;

        // Run the test
        userServiceUnderTest.postAlert(alerts);

        // Verify the results
    }

    @Test
    public void testDeleteAlertById() {
        // Setup
        final Integer id = 0;

        // Run the test
        userServiceUnderTest.deleteAlertById(id);

        // Verify the results
    }

    @Test
    public void testAddAddress() {
        // Setup
        final Address address = null;

        // Run the test
        userServiceUnderTest.addAddress(address);

        // Verify the results
    }

    @Test
    public void testDeleteAddress() {
        // Setup
        final Integer id = 0;

        // Run the test
        userServiceUnderTest.deleteAddress(id);

        // Verify the results
    }

    @Test
    public void testFindAll() {
        // Setup
        final Iterable<Address> expectedResult = null;

        // Run the test
        final Iterable<Address> result = userServiceUnderTest.findAll();

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
