package hu.adhoc.Service;

import hu.adhoc.Dao.GroupDao;
import hu.adhoc.Dao.UserDao;
import hu.adhoc.Entity.KindergardenGroup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class GroupServiceTest {

    private GroupService groupServiceUnderTest;

    @BeforeEach
    public void setUp() {
        groupServiceUnderTest = new GroupService();
        groupServiceUnderTest.groupDao = mock(GroupDao.class);
        groupServiceUnderTest.userDao = mock(UserDao.class);
    }

    @Test
    public void testGetMembers() {
        // Setup
        final Integer groupId = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.getMembers(groupId);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetGroups() {
        // Setup
        final Iterable<KindergardenGroup> expectedResult = null;

        // Run the test
        final Iterable<KindergardenGroup> result = groupServiceUnderTest.getGroups();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetGroupById() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.getGroupById(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetTeachers() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.getTeachers(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetAlertsFromGroupById() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.getAlertsFromGroupById(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetEventsFromGroupById() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.getEventsFromGroupById(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testAddGroup() {
        // Setup
        final KindergardenGroup group = null;

        // Run the test
        groupServiceUnderTest.addGroup(group);

        // Verify the results
    }

    @Test
    public void testDeleteGroup() {
        // Setup
        final Integer id = 0;
        final ResponseEntity expectedResult = null;

        // Run the test
        final ResponseEntity result = groupServiceUnderTest.deleteGroup(id);

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
