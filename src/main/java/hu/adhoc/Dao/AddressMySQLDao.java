package hu.adhoc.Dao;

import hu.adhoc.Entity.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressMySQLDao extends CrudRepository<Address, Integer> {
}
