package hu.adhoc.Dao;

import hu.adhoc.Entity.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ChildDaoMySQLImpl implements ChildDao{
    @Autowired
    ChildMySQLDao childRepo;

    @Override
    public Iterable<Child> findAllChild() {
        return childRepo.findAll();
    }

    @Override
    public void addChild(Child child) {
        childRepo.save(child);
    }

    @Override
    public Optional<Child> findByid(Integer id) {
        return childRepo.findById(id);
    }

    @Override
    public void deleteChild(Integer id) {
        Optional<Child> child = childRepo.findById(id);
        childRepo.delete(child.get());
    }
}
