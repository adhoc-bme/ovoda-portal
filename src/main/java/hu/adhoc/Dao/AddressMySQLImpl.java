package hu.adhoc.Dao;

import hu.adhoc.Entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AddressMySQLImpl implements AddressDao {
    @Autowired
    AddressMySQLDao addressRepo;

    @Override
    public void addAddress(Address address) {
        addressRepo.save(address);
    }

    @Override
    public void deleteAddress(Integer id) {
        Optional<Address> address = addressRepo.findById(id);
        if (address.isPresent())
            addressRepo.delete(address.get());
    }

    @Override
    public Iterable<Address> findAll() {
        return addressRepo.findAll();
    }
}
