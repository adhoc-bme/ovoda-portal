package hu.adhoc.Dao;

import hu.adhoc.Entity.Events;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class EventsDaoMySQLImpl implements EventsDao{
    @Autowired
    EventsMySQLDao eventsRepo;

    @Override
    public void addEvent(Events events) {
        eventsRepo.save(events);
    }

    @Override
    public void deleteEvent(Integer id) {
        Optional<Events> events = eventsRepo.findById(id);
        eventsRepo.delete(events.get());
    }
}
