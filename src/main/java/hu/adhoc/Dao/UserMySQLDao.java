package hu.adhoc.Dao;

import hu.adhoc.Entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserMySQLDao extends CrudRepository<User, Integer> {
    @Query("SELECT id, name, phone FROM User")
    Iterable<User> returnAllUsers();

    @Query("SELECT id, name from User WHERE id=:id")
    Optional<User> returnUserById(@Param("id") Integer id);

    @Query("SELECT id from User where name=:name")
    Integer findIdFromName(@Param("name") String name);

    @Query("SELECT id, name, phone, address FROM User where role_id=2")
    Iterable<User> findAllTeachers();

    @Query("SELECT id, name, phone, address FROM User where role_id=1")
    Iterable<User> findAllParents();

    @Query("SELECT id, name FROM User where kindergarden_group_id=0")
    Iterable<User> returnAllMicimacko();

    @Query("select id, name from User where role_id=2 AND kindergarden_group_id=:id")
    Iterable<User> findAllTeachersByGroupId(@Param("id") Integer id);
}
