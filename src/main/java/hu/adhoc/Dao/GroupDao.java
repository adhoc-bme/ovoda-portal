package hu.adhoc.Dao;

import hu.adhoc.Entity.*;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface GroupDao {
    Collection<Child> getMembers(Integer groupId);

    Iterable<KindergardenGroup> getGroups();

    Optional<KindergardenGroup> getGroupById(Integer id);

    Collection<User> findAllTeachers(Integer id);

    Iterable<Alerts> findAllAlertsByGroupId(Integer id);

    Iterable<Events> findAllEventsByGroupId(Integer id);

    void addGroup(KindergardenGroup group);

    void deleteGroup(Integer id);
}
