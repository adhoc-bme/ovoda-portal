package hu.adhoc.Dao;

import hu.adhoc.Entity.Child;
import org.springframework.data.repository.CrudRepository;

public interface ChildMySQLDao extends CrudRepository<Child, Integer> {
}
