package hu.adhoc.Dao;

import hu.adhoc.Entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
@Qualifier("mysql")
public class GroupDaoMySQLImpl implements GroupDao {
    @Autowired
    GroupMySQLDao groupRepo;

    @Override
    public Collection<Child> getMembers(Integer groupId) {
        return null;
    }

    @Override
    public Iterable<KindergardenGroup> getGroups() {
        return groupRepo.findAll();
    }

    @Override
    public Optional<KindergardenGroup> getGroupById(Integer id) {
        return groupRepo.findById(id);
    }

    @Override
    public Collection<User> findAllTeachers(Integer id) {
        return null;
    }

    @Override
    public Iterable<Alerts> findAllAlertsByGroupId(Integer id) {
        return groupRepo.findAllAlertsByGroupId(id);
    }

    @Override
    public Iterable<Events> findAllEventsByGroupId(Integer id) {
        return groupRepo.findAllEventsByGroupId(id);
    }

    @Override
    public void addGroup(KindergardenGroup group) {
        groupRepo.save(group);
    }

    @Override
    public void deleteGroup(Integer id) {
        Optional<KindergardenGroup> group= groupRepo.findById(id);
        groupRepo.delete(group.get());
    }
}
