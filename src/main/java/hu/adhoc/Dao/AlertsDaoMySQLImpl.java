package hu.adhoc.Dao;

import hu.adhoc.Entity.Alerts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AlertsDaoMySQLImpl implements AlertsDao {
    @Autowired
    AlertsMySQLDao alertsRepo;

    @Override
    public void addAlert(Alerts alert) {
        alertsRepo.save(alert);
    }

    @Override
    public void deleteAlert(Integer id) {
        Optional<Alerts> alert = alertsRepo.findById(id);
        alertsRepo.delete(alert.get());
    }
}
