package hu.adhoc.Dao;

import hu.adhoc.Entity.Alerts;
import org.springframework.data.repository.CrudRepository;

public interface AlertsMySQLDao extends CrudRepository<Alerts, Integer> {
}
