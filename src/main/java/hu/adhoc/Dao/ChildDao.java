package hu.adhoc.Dao;

import hu.adhoc.Entity.Child;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChildDao {
    Iterable<Child> findAllChild();

    void addChild(Child child);

    Optional<Child> findByid(Integer id);

    void deleteChild(Integer id);
}
