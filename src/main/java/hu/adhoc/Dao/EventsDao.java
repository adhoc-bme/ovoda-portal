package hu.adhoc.Dao;

import hu.adhoc.Entity.Events;
import org.springframework.stereotype.Repository;

@Repository
public interface EventsDao {

    void addEvent(Events events);

    void deleteEvent(Integer id);
}
