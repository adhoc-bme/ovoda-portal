package hu.adhoc.Dao;

import hu.adhoc.Entity.Alerts;
import hu.adhoc.Entity.Events;
import hu.adhoc.Entity.KindergardenGroup;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface GroupMySQLDao extends CrudRepository<KindergardenGroup, Integer> {

    @Query("select alerts from KindergardenGroup where kindergarden_group_id=:id")
    Iterable<Alerts> findAllAlertsByGroupId(@Param("id") Integer id);

    @Query("select events from KindergardenGroup where kindergarden_group_id=:id")
    Iterable<Events> findAllEventsByGroupId(@Param("id") Integer id);
}
