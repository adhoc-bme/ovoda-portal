package hu.adhoc.Dao;

import hu.adhoc.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Qualifier("mysql")
public class UserDaoMySQLImpl implements UserDao {
    @Autowired
    UserMySQLDao userRepo;

    @Override
    public Iterable<User> getAllTeachers() {
        return userRepo.findAllTeachers();
    }

    @Override
    public Iterable<User> getAllParents() {
        return userRepo.findAllParents();
    }

    @Override
    @Transactional
    public Iterable<User> getAllUser(){
        return userRepo.findAll();
    }

    @Override
    @Transactional
    public Optional<User> findById(Integer id) {
        return userRepo.findById(id);
    }

    @Override
    public User getUserByName(String name) {
        return null;
    }

    @Override
    @Transactional
    public void deleteUser(Integer id) {
        Optional<User> user =userRepo.findById(id);
        userRepo.delete(user.get());
    }

    @Override
    public void updateUser(User user) {

    }

    @Override
    @Transactional
    public void addUser(User user) {
        userRepo.save(user);
    }

    @Override
    public boolean login(String name) {
        Integer id = userRepo.findIdFromName(name);
        return userRepo.existsById(id);
    }

    @Override
    public Iterable<User> getMicimacko(){
        return userRepo.returnAllMicimacko();
    }

    @Override
    public Iterable<User> findAllTeachersByGroupId(Integer id) {
        return userRepo.findAllTeachersByGroupId(id);
    }


}
