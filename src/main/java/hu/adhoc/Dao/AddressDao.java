package hu.adhoc.Dao;

import hu.adhoc.Entity.Address;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressDao {

    void addAddress(Address address);

    void deleteAddress(Integer id);

    Iterable<Address> findAll();
}
