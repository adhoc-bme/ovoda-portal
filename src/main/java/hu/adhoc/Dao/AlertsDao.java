package hu.adhoc.Dao;

import hu.adhoc.Entity.Alerts;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertsDao {

    void addAlert(Alerts alert);

    void deleteAlert(Integer id);
}
