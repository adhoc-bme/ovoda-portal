package hu.adhoc.Dao;

import hu.adhoc.Entity.Role;
import hu.adhoc.Entity.User;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UserDao {

    Iterable<User> getAllTeachers();

    Iterable<User> getAllParents();

    Iterable<User> getAllUser();

    Optional<User> findById(Integer id);

    User getUserByName(String name);

    void deleteUser(Integer id);

    void updateUser(User user);

    void addUser(User user);

    boolean login(String name);

    Iterable<User> getMicimacko();

    Iterable<User> findAllTeachersByGroupId(Integer id);
}
