package hu.adhoc.Dao;

import hu.adhoc.Entity.Events;
import org.springframework.data.repository.CrudRepository;

public interface EventsMySQLDao extends CrudRepository<Events, Integer> {
}
