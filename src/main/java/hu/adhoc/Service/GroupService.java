package hu.adhoc.Service;

import hu.adhoc.Dao.GroupDao;
import hu.adhoc.Dao.UserDao;
import hu.adhoc.Entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.Optional;

@Service
public class GroupService {
    @Autowired
    @Qualifier("mysql")
    GroupDao groupDao;

    @Autowired
    @Qualifier("mysql")
    UserDao userDao;

    @ResponseBody
    public ResponseEntity getMembers(Integer groupId) {
        Collection<Child> members = groupDao.getMembers(groupId);
        if (members == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("KindergardenGroup not found");
        else
            return new ResponseEntity(members, HttpStatus.OK);
    }

    public Iterable<KindergardenGroup> getGroups() {
        return groupDao.getGroups();
    }

    @ResponseBody
    public ResponseEntity getGroupById(Integer id) {
        Optional<KindergardenGroup> group = groupDao.getGroupById(id);
        if (group == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("KindergardenGroup not found");
        else
            return new ResponseEntity(group, HttpStatus.OK);
    }

    @ResponseBody
    public ResponseEntity getTeachers(Integer id) {
        Iterable<User> teachers = userDao.findAllTeachersByGroupId(id);
        if(teachers == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("KindergardenGroup not found");
        else
            return new ResponseEntity(teachers, HttpStatus.OK);
    }

    @ResponseBody
    public ResponseEntity getAlertsFromGroupById(Integer id) {
        Iterable<Alerts> alerts = groupDao.findAllAlertsByGroupId(id);
        if(alerts == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("KindergardenGroup not found");
        else
            return new ResponseEntity(alerts, HttpStatus.OK);
    }

    @ResponseBody
    public ResponseEntity getEventsFromGroupById(Integer id) {
        Iterable<Events> events = groupDao.findAllEventsByGroupId(id);
        if(events == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("KindergardenGroup not found");
        else
            return new ResponseEntity(events, HttpStatus.OK);
    }


    public void addGroup(KindergardenGroup group){
        groupDao.addGroup(group);
    }

    @ResponseBody
    public ResponseEntity deleteGroup(Integer id) {
        ResponseEntity group = getGroupById(id);
        if(group.getStatusCode() == HttpStatus.NOT_FOUND)
            return group;
        else {
            groupDao.deleteGroup(id);
            return new ResponseEntity(HttpStatus.OK);
        }
    }
}
