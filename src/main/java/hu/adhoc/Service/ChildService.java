package hu.adhoc.Service;

import hu.adhoc.Dao.ChildDao;
import hu.adhoc.Entity.Child;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChildService {
    @Autowired
    ChildDao childDao;

    public Iterable<Child> findAllChild() {
        return childDao.findAllChild();
    }

    public void addChild(Child child) {
        childDao.addChild(child);
    }

    public Optional<Child> findById(Integer id) {
        return childDao.findByid(id);
    }

    public void deleteChild(Integer id) {
        childDao.deleteChild(id);
    }
}
