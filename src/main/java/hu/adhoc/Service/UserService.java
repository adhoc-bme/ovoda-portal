package hu.adhoc.Service;

import hu.adhoc.Dao.*;
import hu.adhoc.Entity.Address;
import hu.adhoc.Entity.Alerts;
import hu.adhoc.Entity.Events;
import hu.adhoc.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    @Qualifier("mysql")
    UserDao userDao;

    @Autowired
    EventsDao eventsDao;

    @Autowired
    AlertsDao alertsDao;

    @Autowired
    AddressDao addressDao;

    public Iterable<User> getAllParents(){
        return userDao.getAllParents();
    }

    public Iterable<User> getAllTeachers(){
        return userDao.getAllTeachers();
    }

    @ResponseBody
    public ResponseEntity getUserById(Integer id){
        Integer integer = 0;
        if(id.getClass() != integer.getClass())
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Invalid ID supplied");
        Optional<User> user = userDao.findById(id);
        if(user == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("User not found");
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @ResponseBody
    public ResponseEntity deleteUser(Integer id) {
        Integer integer = 0;
        if(id.getClass() != integer.getClass())
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Invalid ID supplied");
        Optional<User> user = userDao.findById(id);
        if(user == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Parent not found");
        userDao.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public void updateUser(User user){
        userDao.updateUser(user);
    }

    public void addUser(User user) {
        userDao.addUser(user);
    }

    @ResponseBody
    public ResponseEntity login(String name) {
        if (userDao.login(name)){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Successful login");
        }
        else
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Invalid username");
    }

    @ResponseBody
    public ResponseEntity logout(){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Successful logout");
    }

    /*@ResponseBody
    public ResponseEntity getGroupByUserId(Integer id) {
        Integer integer = 0;
        if(id.getClass() != integer.getClass())
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Invalid ID supplied");
        User user = userDao.findById(id);
        if(user == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("User not found");
        return new ResponseEntity<>(user.getKindergardenGroup(), HttpStatus.OK);
    }

    @ResponseBody
    public ResponseEntity shareWithParent(Integer id) {
        User user = userDao.findById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Shared with:" + user.getName());
    }*/

    public Iterable<User> getAllUsers(){
        return userDao.getAllUser();
    }

    public Iterable<User> getMicimacko() { return userDao.getMicimacko();
    }

    public void postEvent(Events event) {
        eventsDao.addEvent(event);
    }

    public void deleteEventById(Integer id){
        eventsDao.deleteEvent(id);
    }

    public void postAlert(Alerts alerts){
        alertsDao.addAlert(alerts);
    }

    public void deleteAlertById(Integer id){
        alertsDao.deleteAlert(id);
    }

    public void addAddress(Address address){ addressDao.addAddress(address);}

    public void deleteAddress(Integer id){addressDao.deleteAddress(id);}

    public Iterable<Address> findAll(){return addressDao.findAll();}
}
