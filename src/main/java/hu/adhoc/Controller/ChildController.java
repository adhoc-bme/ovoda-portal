package hu.adhoc.Controller;

import hu.adhoc.Entity.Child;
import hu.adhoc.Service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.Optional;

@RestController
public class ChildController {
    @Autowired
    ChildService childService;

    @RequestMapping(value = "/children", method = RequestMethod.GET)
    public Iterable<Child> findAllChild(){
        return childService.findAllChild();
    }

    @RequestMapping(value = "/child", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addChild(@RequestBody Child child){
        childService.addChild(child);
    }

    @RequestMapping(value = "/child/{id}", method = RequestMethod.GET)
    public Optional<Child> findById(@PathVariable Integer id){
        return childService.findById(id);
    }

    @RequestMapping(value = "/child/{id}", method = RequestMethod.DELETE)
    public void deleteChild(@PathVariable Integer id){
        childService.deleteChild(id);
    }
}
