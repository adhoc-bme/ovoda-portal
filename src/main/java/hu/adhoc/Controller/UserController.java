package hu.adhoc.Controller;

import hu.adhoc.Entity.Address;
import hu.adhoc.Entity.Alerts;
import hu.adhoc.Entity.Events;
import hu.adhoc.Entity.User;
import hu.adhoc.Messaging.JmsClient;
import hu.adhoc.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    JmsClient jmsClient;

    @RequestMapping(value = "/parents", method = RequestMethod.GET)
    public Iterable<User> getAllParents(){
        return userService.getAllParents();
    }

    @RequestMapping(value = "/teachers", method = RequestMethod.GET)
    public Iterable<User> getAllTeachers(){
        return userService.getAllTeachers();
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity getUserById(@PathVariable Integer id){
        return userService.getUserById(id);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Integer id){
        return userService.deleteUser(id);
    }

    /*@RequestMapping(value = "/user/{id}/group", method = RequestMethod.GET)
    public ResponseEntity getGroupByUserId(@PathVariable Integer id){
        return userService.getGroupByUserId(id);
    }*/

    @RequestMapping(value = "/user" ,method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateUser(@RequestBody User user){
        userService.updateUser(user);
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addUser(@RequestBody User user){
        userService.addUser(user);
    }

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public ResponseEntity loginParent(@RequestBody String name){
        return userService.login(name);
    }

    @RequestMapping(value = "/user/logout", method = RequestMethod.GET)
    public ResponseEntity logoutParent(){
        return userService.logout();
    }

    /*@RequestMapping(value = "/user/shareWithParent/{id}", method = RequestMethod.POST)
    public ResponseEntity shareWithParent(@PathVariable Integer id){return userService.shareWithParent(id);}
    */
    @RequestMapping(value = "/user/event", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void postEvent(@RequestBody Events event){
        userService.postEvent(event);
    };

    @RequestMapping(value = "/user/event/{id}", method = RequestMethod.DELETE)
    public void deleteEvent(@PathVariable Integer id){
        userService.deleteEventById(id);
    };

    @RequestMapping(value = "/user/alert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void postAlert(@RequestBody Alerts alert){
        //TODO
        userService.postAlert(alert);
    }

    @RequestMapping(value = "/user/alert/{id}", method = RequestMethod.DELETE)
    public void deleteAlert(@PathVariable Integer id){
        userService.deleteAlertById(id);
    }

    public void sendMessageToUser(){
        //TODO
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public void sendMessageToAdmin(@RequestBody String message){
        //TODO

    }

    public void sendHomeWorkToParent(){
        //TODO
    }

    @RequestMapping(value = "/address", method = RequestMethod.POST)
    public void addAddress(@RequestBody Address address){
        userService.addAddress(address);
    }

    @RequestMapping(value = "/address/{id}", method = RequestMethod.DELETE)
    public void deleteAddress(@PathVariable Integer id){
        userService.deleteAddress(id);
    }

    @RequestMapping(value = "/address", method = RequestMethod.GET)
    public Iterable<Address> findAll(){return userService.findAll();}

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Iterable<User> getAllUsers(){return userService.getAllUsers();}

    @RequestMapping(value = "/teszt", method = RequestMethod.GET)
    public Iterable<User> getMicimacko(){return userService.getMicimacko();}



}
