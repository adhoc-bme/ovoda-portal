package hu.adhoc.Controller;

import hu.adhoc.Entity.KindergardenGroup;
import hu.adhoc.Service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.security.util.ManifestEntryVerifier;

@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    GroupService groupService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<KindergardenGroup> getGroups(){
        return groupService.getGroups();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getGroupById(@PathVariable Integer id){
        return groupService.getGroupById(id);
    }

    @RequestMapping(value = "/members/{id}", method = RequestMethod.GET)
    public ResponseEntity getMembers(@PathVariable Integer id){
        return groupService.getMembers(id);
    }

    @RequestMapping(value = "/teachers/{id}", method = RequestMethod.GET)
    public ResponseEntity getTeachers(@PathVariable Integer id){
        return groupService.getTeachers(id);
    }

    @RequestMapping(value = "/alerts/{id}", method = RequestMethod.GET)
    public ResponseEntity getAlertsFromGroupById(@PathVariable Integer id){
        return groupService.getAlertsFromGroupById(id);
    }

    @RequestMapping(value = "/events/{id}", method = RequestMethod.GET)
    public ResponseEntity getEventsFromGroupById(@PathVariable Integer id){
        return groupService.getEventsFromGroupById(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addGroup(@RequestBody KindergardenGroup group){
        groupService.addGroup(group);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteGroup(@PathVariable Integer id){
        return groupService.deleteGroup(id);
    }
}
