package hu.adhoc.Entity;


import javax.persistence.*;
import java.util.Collection;
@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;
    private String name;
    /*@OneToMany(targetEntity=User.class, mappedBy="role", fetch=FetchType.EAGER)
    private Collection<User> users;
    @OneToMany(targetEntity=Privilege.class, mappedBy="roles", fetch=FetchType.EAGER)
    private Collection<Privilege> privileges;
*/
    public Role(Integer id, String name) {
        Id = id;
        this.name = name;
    }

    public Role() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
