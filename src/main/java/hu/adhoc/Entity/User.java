package hu.adhoc.Entity;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer phone;
    private String email;

    @OneToOne(targetEntity=Role.class)
    private Role role;

    @OneToOne(targetEntity= KindergardenGroup.class)
    private KindergardenGroup kindergardenGroup;

    @OneToOne(targetEntity=Address.class)
    private Address address;

    public User(Integer id, String name, Integer phone, String email, Role role) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.role = role;
    }

    public User(Integer id, String name, Integer phone, String email, Role role, KindergardenGroup kindergardenGroup) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.role = role;
        this.kindergardenGroup = kindergardenGroup;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public KindergardenGroup getKindergardenGroup() {
        return kindergardenGroup;
    }

    public void setKindergardenGroup(KindergardenGroup kindergardenGroup) {
        this.kindergardenGroup = kindergardenGroup;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
