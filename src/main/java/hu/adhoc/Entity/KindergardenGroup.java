package hu.adhoc.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
public class KindergardenGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @OneToMany(targetEntity=Child.class)
    private List<Child> members;
    @Column(name = "name")
    private String name;
    @OneToMany(targetEntity=Alerts.class)
    private List<Alerts> alerts;
    @OneToMany(targetEntity=Events.class)
    private List<Events> events;

    public KindergardenGroup(){
        members = new ArrayList<>();
        alerts = new ArrayList<>();
        events = new ArrayList<>();
    }

    public KindergardenGroup(Integer id, List<Child> members, String name) {
        this.id = id;
        this.members = members;
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Child> getMembers() {
        return members;
    }

    public void setMembers(List<Child> members) {
        this.members = members;
    }

    public List<Alerts> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<Alerts> alerts) {
        this.alerts = alerts;
    }

    public List<Events> getEvents() {
        return events;
    }

    public void setEvents(List<Events> events) {
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
