package hu.adhoc.Messaging;

public interface JmsClient {
	public void send(String msg);
	public String receive();
}