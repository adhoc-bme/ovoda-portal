import { TestBed } from '@angular/core/testing';

import { KindergartenService } from './kindergarten.service';

describe('KindergartenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KindergartenService = TestBed.get(KindergartenService);
    expect(service).toBeTruthy();
  });
});
