import {Injectable, OnInit} from '@angular/core';
import {Http} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class KindergartenService implements OnInit{
  private url = 'http://145.236.179.22:53498/';
    /*'http://localhost:53498/';*/


  constructor(private http: Http) {
    console.log('KindergartenService constructor');
  }

  ngOnInit(){
    console.log('KindergartenService init');
  }

  public getAllChildren(){
    return this.http.get(this.url + 'api/Child/listAllChildren');
  }

  public getChildrenWithoutGroup(){
    return this.http.get(this.url + 'api/Child/childrenWithoutGroup');
  }

  public getChildrenWithoutParent(){
    return this.http.get(this.url + 'api/Child/childrenWithoutParent');
  }

  public createChildid(id: number, name: string, group_id: number, parent_id: number, sign: string){
    return this.http.post(this.url + 'api/Child/createChild', {id: id, name: name, groupId: group_id, parentId: parent_id, sign: sign});
  }

  public updateChild(id: number, name: string, group_id: number, parent_id: number, sign: string){
    return this.http.post(this.url + 'api/Child/updateChild', {id: id, name: name, groupId: group_id, parentId: parent_id, sign: sign});
  }

  public deleteChildById(id: number){
    return this.http.delete(this.url + 'api/Child/delete?childID=' + id);
  }

  public getUserById(id: number) {
    return this.http.get(this.url + 'api/User/findUserById?userID=' + id);
  }
  public getAllUsers(){
    return this.http.get(this.url + 'api/User/listAllUsers');
  }

  public  deleteUserById(id: number){
    return this.http.delete(this.url + 'api/User/delete?userID=' + id);
  }

  public updateUser(user){
    return this.http.post(this.url + 'api/User/update', user);
  }

  public getPotentialParents(){
    return this.http.get(this.url + 'api/User/listPotentialParents');
  }

  public getSimpleUsersByIds(ids: Array<number>){
    return this.http.get(this.url + 'api/User/getSimpleUsersByIds');
    /*TODO*/
  }

  public unassignTeacherFromGroup(teacher_id: number){
    return this.http.post(this.url + 'api/User/unassignTeacherFromGroup?teacherID=' + teacher_id, '');
  }

  public getPostsByGroupId(id: number){
    return this.http.get(this.url + 'api/Group/getPostsOfGroup?groupID=' + id);
  }

  public createPost(msg: string, poster_id: number, date: string, has_image: boolean, group_id: number){
    return this.http.post(this.url + 'api/Post/createPost',{id: 0, message: msg, posterId: poster_id, date: date, hasImage: has_image, groupId: group_id});
  }

  public getGroupById(id: number){
    return this.http.get(this.url + 'api/Group/findGroupById?groupID=' + id);
  }

  public deleteGroupById(id: number){
    return this.http.delete(this.url + 'api/Group/delete?groupID=' + id);
  }

  public getRelevantGroupsById(id: number) {
    return this.http.get(this.url + 'api/Group/getRelevantGroups?userID=' + id);
  }

  public createGroup(name: string){
    return this.http.get(this.url + 'api/Group/createGroup?name=' + name);
  }

  public getPotentialTeachersForGroup(group_id: number){
    return this.http.get(this.url + 'api/User/listPotentialTeachersForGroup?groupID=' + group_id);
  }

  public assignTeachersToGroup(group_id:  number, teacher_ids: Array<number>){
    return this.http.post(this.url + 'api/User/assignTeachersToGroup?groupID=' + group_id, teacher_ids);
  }

  public sendNewAlert(msg: string, due_date: string, group_id: number) {
    return this.http.post(this.url + 'api/Alert/createAlert', {id: 1 , message: msg, dueDate: due_date, groupId: group_id});
  }

  public deleteAlertById(id: number){
    return this.http.delete(this.url + 'api/Alert/deleteAlert?alertID=' + id);
  }

  public getUserMessagePartnersById(id: number){
    return this.http.get(this.url + 'api/User/listMessagingPartnersOfUser?userID=' + id);
  }

  public getMessagesBetweenUsers(current: number, target: number){
    return this.http.get(this.url + 'api/Message/listMessagesBetweenUsers?current=' + current + '&target=' + target);
  }

  public sendMessage(from: number, to: number, message: string, hasImage: boolean){
    return this.http.post(this.url + 'api/Message/createMessage', {id: -1, from: from, to: to, message: message, hasImage: hasImage});
  }

  public getAllGroups(){
    return this.http.get(this.url + 'api/Group/listAllGroups');
  }

  public getGroupsByUserId(id: number){
    return this.http.get(this.url + 'api/Group/getRelevantGroups?userID={' + id + '}');
  }
  getSimpleGroupList(){
    return this.http.get(this.url + 'api/Group/getSimpleGroupList');
  }
  getSimpleGroupListByUserId(id: number){
    return this.http.get(this.url + 'api/Group/getSimpleGroupById?groupID={' + id + '}');
  }

  /*LOGIN*/

  login(username: string, pass: string){
    return this.http.get(this.url + 'api/User/logInAs?username=' + username + '&password=' + pass);
  }

  register(username: string, pass: string){
    return this.http.get(this.url + 'api/User/register?username=' + username + '&password=' + pass);
  }



 /* getChildById(id: number){
    for (let child of this._children) {
      if (child.child_id === id) {
        return child;
      }
    }
  }

  getChildrenByGroupId(g_id){
    let temp_children = Array<Child>();
    for (let child of this._children) {
      if (child.group_id === g_id) {
        temp_children.push(child);
      }
    }
    return temp_children;
  }

  get children(): Array<Child> {
    return this._children;
  }*/

/*  getNonGroupChildren(){
    let tempA = new Array<Child>();
    for(let child of this._children){
      if(child.group_id === -1){
        tempA.push(child);
      }
    }
    return tempA;
  }
  getNonParentChildren(){
    let tempA = new Array<Child>();
    for(let child of this._children){
      if(child.parent_id === -1){
        tempA.push(child);
      }
    }
    return tempA;
  }

  setChildGroup(group_id: number, child_id){
    for (let child of this._children) {
      if (child.child_id === child_id) {
        child.group_id = group_id;
        break;
      }
    }
  }*/

}
