import {Injectable, OnInit} from '@angular/core';
import {KindergartenService} from './kindergarten.service';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit{


  current: Account;

  constructor(private service: KindergartenService) {
  }

  ngOnInit(){
    console.log('user service OnInit');
  }

  isLoggedIn(){
    if (localStorage.getItem('userId') !== null) {
      return true;
    } else {
      return false;
    }
  }

  login(id: number, name: string, email: string, phone: string, role: string, address: {zip: number, street: string, town: string}, hasImage: boolean){
    this.current = new Account(id, name, email, phone, role, {zip: address.zip, street: address.street, town: address.town}, hasImage);
    localStorage.setItem('userId', this.current.id.toString());
    localStorage.setItem('role', this.current.role.toString());
  }

  logout(){
    localStorage.removeItem('userId');
    localStorage.removeItem('role');
  }

  getRole(){
    return +localStorage.getItem('role');
  }

  getUserId(){
    return +localStorage.getItem('userId');
  }


  getAccount(){
    return {id: this.current.id, name: this.current.name, email: this.current.email, phone: this.current.phone, role: this.current.role, address: this.current.address, hasImage: this.current.hasImage};
  }

  private refresh(){
    if (localStorage.getItem('userId') !== null){
      this.service.getUserById(+localStorage.getItem('userId')).subscribe(response => {
        if(response.status === 200){
          let acc = response.json();
          this.current = new Account(acc.id, acc.name, acc.email, acc.phone, acc.role, {zip: acc.address.zip, street: acc.address.street, town: acc.address.town}, acc.hasImage);
        } else {
          /*TODO*/
        }
      });
    }
  }

}

class Account{

  private _name: string;
  private _email: string;
  private _phone: string;
  private _address: {zip: number, street: string, town: string};
  private _hasImage: boolean;
  private _id: number;
  private _role: Role;

  constructor(id: number, name: string, email: string, phone: string, role: string, address: {zip: number, street: string, town: string}, hasImage: boolean){
    this._id = id;
    this._name = name;
    this._email = email;
    this._phone = phone;
    this._address = address;
    this._hasImage = hasImage;
    if(role === 'ADMIN'){
      this._role = Role.ADMIN;
    } else if(role === 'TEACHER'){
      this._role = Role.TEACHER;
    } else if (role === 'USER') {
      this._role = Role.USER;
    } else {
      this._role = Role.NOROLE;
    }
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(value: string) {
    this._phone = value;
  }

  get address(): { zip: number; street: string; town: string } {
    return this._address;
  }

  set address(value: { zip: number; street: string; town: string }) {
    this._address = value;
  }

  get hasImage(): boolean {
    return this._hasImage;
  }

  set hasImage(value: boolean) {
    this._hasImage = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get role(): Role {
    return this._role;
  }

  set role(value: Role) {
    this._role = value;
  }

}

export enum Role {
  'ADMIN',
  'TEACHER',
  'USER',
  'NOROLE'
}
