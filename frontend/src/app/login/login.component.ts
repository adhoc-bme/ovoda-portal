import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {KindergartenService} from '../services/kindergarten.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name: string;
  pass: string;
  display_register: boolean;

  constructor(
    private kd_service: KindergartenService,
    private service: UserService,
    private router: Router
  ){}

  ngOnInit() {
    this.display_register = false;
    this.name = '';
    this.pass = '';
  }

  login() {
    this.kd_service.login(this.name, this.pass).subscribe(response => {
      if(response.status === 200){
        let acc = response.json();
        this.service.login(acc.id, acc.name, acc.email, acc.phone, acc.role, {zip: acc.address.zip, street: acc.address.street, town: acc.address.town}, acc.hasImage);
        this.router.navigate(['/main']);
      } else {
        console.log('failed');
        /*TODO */
      }
    });
    /*TODO*/
  }

  displayRegister(){
    this.display_register = true;
  }

  finishRegister(){
    this.display_register = false;
  }
}
