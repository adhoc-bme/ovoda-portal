import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {KindergartenService} from '../../services/kindergarten.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() finish = new EventEmitter();
  pass0;
  pass1;
  username;

  constructor(
    private service: KindergartenService
  ) { }

  ngOnInit() {
    this.pass0 = null;
    this.username = null;
    this.pass1 = null;
  }

  register(){
    if(this.username !== '' && this.pass0 === this.pass1){
      this.service.register(this.username, this.pass0).subscribe(response => {
      });
      this.finish.emit();
    } else {
      console.log('Üres név, vagy a jelszók nem egyeznek');
    }


  }
  cancel(){
    this.pass0 = null;
    this.username = null;
    this.pass1 = null;
    this.finish.emit();
  }

}
