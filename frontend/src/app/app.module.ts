import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AccordionModule} from 'primeng/accordion';     // accordion and accordion tab
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {TableModule} from 'primeng/table';
import {
  ButtonModule,
  CheckboxModule, DataTableModule, DialogModule, DropdownModule,
  InputTextModule,
  MegaMenuModule,
  MenubarModule,
  MenuModule, MessageModule, MessagesModule,
  PasswordModule, ScrollPanelModule,
  TabMenuModule
} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MainComponent } from './main/main.component';
import { MessagesComponent } from './main/messages/messages.component';
import { ProfileComponent } from './main/profile/profile.component';
import {GroupsComponent} from './main/groups/groups.component';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import { FormsModule } from '@angular/forms';
import { MembersComponent } from './main/groups/members/members.component';
import { AlertsComponent } from './main/groups/alerts/alerts.component';
import { SharedDocumentsComponent } from './main/groups/shared-documents/shared-documents.component';
import { MessageBoardComponent } from './main/groups/message-board/message-board.component';
import {UserService} from './services/user.service';
import { ReportComponent } from './main/report/report.component';
import { AdminComponent } from './main/admin/admin.component';
import { AdminGroupsComponent } from './main/admin/admin-groups/admin-groups.component';
import { AdminUsersComponent } from './main/admin/admin-users/admin-users.component';
import {AdminChildrenComponent} from './main/admin/admin-children/admin-children.component';
import { GroupChildrenComponent } from './main/admin/admin-groups/group-children/group-children.component';
import { GroupTeachersComponent } from './main/admin/admin-groups/group-teachers/group-teachers.component';
import { SelectChildrenComponent } from './main/admin/admin-groups/group-children/select-children/select-children.component';
import {DataViewModule} from 'primeng/dataview';
import { EditChildComponent } from './main/admin/edit-child/edit-child.component';
import {HttpModule} from '@angular/http';
import { GroupAlertsComponent } from './main/admin/admin-groups/group-alerts/group-alerts.component';
import {RouterModule} from '@angular/router';
import { ChatComponent } from './main/messages/chat/chat.component';
import {AutosizeModule} from 'ngx-autosize';
import { SelectTeachersComponent } from './main/admin/admin-groups/group-teachers/select-teachers/select-teachers.component';
import { NewChildComponent } from './main/admin/admin-children/new-child/new-child.component';
import { ChildEditorComponent } from './main/admin/admin-children/child-editor/child-editor.component';
import { UserEditorComponent } from './main/admin/admin-users/user-editor/user-editor.component';
import { NewPartnerComponent } from './main/messages/new-partner/new-partner.component';
import { RegisterComponent } from './login/register/register.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    MessagesComponent,
    ProfileComponent,
    GroupsComponent,
    MembersComponent,
    AlertsComponent,
    SharedDocumentsComponent,
    MessageBoardComponent,
    ReportComponent,
    AdminComponent,
    AdminGroupsComponent,
    AdminChildrenComponent,
    AdminUsersComponent,
    GroupChildrenComponent,
    GroupTeachersComponent,
    SelectChildrenComponent,
    EditChildComponent,
    GroupAlertsComponent,
    ChatComponent,
    SelectTeachersComponent,
    NewChildComponent,
    ChildEditorComponent,
    UserEditorComponent,
    NewPartnerComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    MenuModule,
    MenubarModule,
    BrowserAnimationsModule,
    PasswordModule,
    InputTextModule,
    CheckboxModule,
    TabMenuModule,
    MegaMenuModule,
    TableModule,
    InputTextareaModule,
    EditorModule,
    FormsModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    DataTableModule,
    DropdownModule,
    ScrollPanelModule,
    DialogModule,
    DataViewModule,
    HttpModule,
    AutosizeModule,
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'main',
        component: MainComponent,
        children: [
          {
            path: 'groups',
            component: GroupsComponent,
            children: [
              {
                path: 'members/:groupId',
                component: MembersComponent
              },
              {
                path: 'message-board/:groupId',
                component: MessageBoardComponent
              },
              {
                path: 'alerts/:groupId',
                component: AlertsComponent
              },
              {
                path: 'documents/:groupId',
                component: SharedDocumentsComponent
              }
            ]
          },
          {
            path: 'messages',
            component: MessagesComponent
          },
          {
            path: 'profile',
            component: ProfileComponent
          },
          {
            path: 'admin',
            component: AdminComponent,
            children: [
              {
                path: 'group-manager',
                component: AdminGroupsComponent,
                children: [
                  {
                    path: 'children/:groupId',
                    component: GroupChildrenComponent
                  },
                  {
                    path: 'teachers/:groupId',
                    component: GroupTeachersComponent
                  },
                  {
                    path: 'alerts/:groupId',
                    component: GroupAlertsComponent
                  }/*,
                  {
                    path: 'documents/:groupId',
                    component: GroupAlertsComponent
                    /!*TODO*!/
                  }*/
                ]
              },
              {
                path: 'child-manager',
                component: AdminChildrenComponent
              },
              {
                path: 'user-manager',
                component: AdminUsersComponent
              }
            ]
          }
        ]
      },
/*      {
        path: '**',
        component: NotFoundComponent
      }*/
    ]),
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
