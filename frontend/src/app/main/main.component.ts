import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  mainMenu: MenuItem[];
  state: number;

  constructor(
    private service: UserService,
    private router: Router
  ) {  }

  ngOnInit() {
    this.state = 0;
    this.mainMenu = [
      {
        label: 'Csoportok',
        routerLink: ['groups'],
        styleClass: 'active current',

      },
      {
        label: 'Üzenetek',
        routerLink: ['messages'],
        styleClass: 'active current'
      },
/*      {label: 'Hibajelentés',
        command: (event) => this.onClickG(2)},*/
      {
        label: 'Profil',
        routerLink: ['profile'],
        styleClass: 'active current'
      },
      {
        label: 'Admin',
        routerLink: ['admin'],
        styleClass: 'active current',
        visible: this.service.getRole() === 0
      }
    ];
  }

  loguot(){
    this.router.navigate(['/login']);
    localStorage.clear();
  }

  isLoggedIn(){
    if(localStorage.getItem('userId') === null) {
      this.loguot();
    }
    return localStorage.getItem('userId') !== null;
  }
}
