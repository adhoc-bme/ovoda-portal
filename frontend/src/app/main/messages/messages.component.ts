import { Component, OnInit } from '@angular/core';
import {KindergartenService} from '../../services/kindergarten.service';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  user_id = -1;
  partner = {id: -1, name: ''};
  text: string;
  partners: Array<{id: number, name: string}>;
  display: boolean;
  display_new_msg: boolean;

  constructor(
    private service: KindergartenService,
    private user_service: UserService
  ) { }



  ngOnInit() {
    this.display = false;
    this.display_new_msg = false;
    this.user_id = this.user_service.getUserId();
    this.partners = [];
    this.service.getUserMessagePartnersById(this.user_id).subscribe(response => {
      for(let u of response.json()){
        this.partners.push({
          id: u.id,
          name: u.name
        });
      }
    });
  }

  openMessage(id: number, name: string){
    this.partner.name = name;
    this.partner.id = id;
    this.display = true;
  }
  newMessagePartner(){
    this.display_new_msg = true;
  }
}
