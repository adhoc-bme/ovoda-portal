import { Component, OnInit } from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-new-partner',
  templateUrl: './new-partner.component.html',
  styleUrls: ['./new-partner.component.css']
})
export class NewPartnerComponent implements OnInit {

  simple_partners: Array<{id: number, name: string}>;
  selected_partner: {id: number, name: string};
  load: boolean;
  msg_text: string;

  constructor(
    private service: KindergartenService
  ) { }

  ngOnInit() {
    this.load = false;
    this.simple_partners = [];
    this.selected_partner = {id: null, name: null};
    this.service.getAllUsers().subscribe(response => {
      for(let u of response.json()){
        if(+localStorage.getItem('userId') !== u.id){
          this.simple_partners.push({id: u.id, name: u.name});
        }
      }
      if(this.simple_partners.length > 0){
        this.selected_partner = this.simple_partners[0];
      }
      this.load = true;
    });
  }

  sendMessage(event){
    this.service.sendMessage(+localStorage.getItem('userId'), this.selected_partner.id, this.msg_text, false)
      .subscribe(response => {
      });
  }

}
