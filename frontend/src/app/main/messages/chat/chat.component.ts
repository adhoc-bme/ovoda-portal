import { Component, OnInit, Input } from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @Input() partner_id: number;
  user_id: number;
  messages: Array<{id: number, from_id: number, to_id: number, message: string, has_image: boolean}>;
  newText: string;

  constructor(
    private service: KindergartenService,
    private user_service: UserService
  ) { }

  ngOnInit() {
    this.user_id = this.user_service.getUserId();
    this.refreshChat();

  }

  sendMessage(){
    if(this.newText !== ''){
      this.service.sendMessage(this.user_id, this.partner_id, this.newText, false).subscribe(response => {
        this.newText = '';
        this.refreshChat();
      });
    }
    /*TODO*/
  }

  refreshChat(){
    this.service.getMessagesBetweenUsers(this.user_id, this.partner_id).subscribe(response => {
      this.messages = [];
      for(let m of response.json()){
        this.messages.unshift({id: m.id, from_id: m.from, to_id: m.to, message: m.message, has_image: m.hasImage});
      }
    });
  }

}
