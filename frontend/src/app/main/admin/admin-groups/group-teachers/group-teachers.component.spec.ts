import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupTeachersComponent } from './group-teachers.component';

describe('GroupTeachersComponent', () => {
  let component: GroupTeachersComponent;
  let fixture: ComponentFixture<GroupTeachersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupTeachersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupTeachersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
