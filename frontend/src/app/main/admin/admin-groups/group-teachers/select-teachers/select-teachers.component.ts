import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {KindergartenService} from '../../../../../services/kindergarten.service';

@Component({
  selector: 'app-select-teachers',
  templateUrl: './select-teachers.component.html',
  styleUrls: ['./select-teachers.component.css']
})
export class SelectTeachersComponent implements OnInit {
  @Input()  group_id: number;
  @Output() finish = new EventEmitter<Array<number>>();

  teachers: Array<{id: number, name: string, group_id: number, email: string, hasImage: boolean, phone: string, address: {zip: number, street: string, town: string}}>;
  selected_teachers: Array<string>;

  constructor(
    private service: KindergartenService,
  ) {
  }

  ngOnInit() {
    this.group_id = 0;
    this.teachers = [];
    this.selected_teachers = [];
    this.refreshTeachers();
  }

  okOnClick() {
    let temp_ids =  new Array<number>()
    for(let str of this.selected_teachers){
      temp_ids.push(+str);
    }
    this.finish.emit(temp_ids);
    this.selected_teachers = [];
    this.refreshTeachers();
  }

  refreshTeachers(){
    this.service.getPotentialTeachersForGroup(this.group_id).subscribe(response => {
      for(let t of response.json()){
        this.teachers.push(
          {
            id: t.id,
            name: t.name,
            group_id: t.groupId,
            email: t.email,
            hasImage: t.hasImage,
            phone: t.phone,
            address: {zip: t.address.zip, street: t.address.street, town: t.address.town}
          });
      }
    });
  }





}
