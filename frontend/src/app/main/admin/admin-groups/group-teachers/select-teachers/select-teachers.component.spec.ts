import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTeachersComponent } from './select-teachers.component';

describe('SelectTeachersComponent', () => {
  let component: SelectTeachersComponent;
  let fixture: ComponentFixture<SelectTeachersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTeachersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTeachersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
