import {Component, OnInit} from '@angular/core';
import {KindergartenService} from '../../../../services/kindergarten.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-group-teachers',
  templateUrl: './group-teachers.component.html',
  styleUrls: ['./group-teachers.component.css']
})
export class GroupTeachersComponent implements OnInit {
  group_id: number;
  teachers: Array<{id: number, name: string, phone: string, email: string, addressObj: {street: string, zip: number, town: string}, address: string}>;
  cols;
  displayPotentialTeachers: boolean


  constructor(
    private service: KindergartenService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.displayPotentialTeachers = false;
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshTeachers();
    });
    this.cols = [
      { field: 'name', header: 'Név' },
      { field: 'phone', header: 'Telefon' },
      { field: 'email', header: 'E-mail' },
      { field: 'address', header: 'Cím' }
    ];
  }

  removeTeacherFromGroup(id: number) {
    this.service.unassignTeacherFromGroup(id).subscribe(response => {
      if(response.json() !== 404) {
        for (let t of this.teachers) {
          if (t.id === id) {
            this.teachers.slice(this.teachers.indexOf(t), 1);
          }
        }
      }
  });
  }


  refreshTeachers(){
    this.service.getGroupById(this.group_id).subscribe(response => {
      this.teachers = [];
      for(let t of response.json().teachers){
        this.teachers.push({
          id: t.id,
          name: t.name,
          phone: t.phone,
          email: t.email,
          addressObj: {
            zip: t.address.zip,
            street: t.address.street,
            town: t.address.town},
          address: (t.address.town + ' ' + t.address.street + ' ' + t.address.zip)
        });
      }
    });
  }

  setdisplayPotentialTeachers(){
    this.displayPotentialTeachers = true;
  }

  onFinish(teachers: Array<number>){
    for(let t of teachers){
      this.service.assignTeachersToGroup(this.group_id, teachers).subscribe(response => {
      });
    }
    this.refreshTeachers();
    this.displayPotentialTeachers = false;
  }
}
