import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {Router} from '@angular/router';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-admin-groups',
  templateUrl: './admin-groups.component.html',
  styleUrls: ['./admin-groups.component.css']
})
export class AdminGroupsComponent implements OnInit {

  tabMenu: MenuItem[];
  groups;
  selectedGroup;
  displayNewGroup: boolean;
  new_group_name: string;

  constructor(
    private service: KindergartenService,
    private router: Router) {
  }

  ngOnInit() {
    console.log('main-admin-group-manager');
    this.selectedGroup = {name: '', id: -1};
    this.refreshGroupList();
  }


  onClickTM() {
    this.refreshTabMenu();
    this.router
      .navigate(['/main/admin/group-manager/children', this.selectedGroup.id])
      .catch( reason => {
    });
  }

  onClickNewGroup(){
    this.displayNewGroup = true;
  }

  cancelNewGroup() {
    this.displayNewGroup = false;
    this.new_group_name = '';
  }

  createNewGroup() {
    this.service.createGroup(this.new_group_name).subscribe(response => {
    });
    this.displayNewGroup = false;
  }

  onClickDeleteGroup() {
    this.service.deleteGroupById(this.selectedGroup.id).subscribe(response => {
    });
  }

  refreshTabMenu(){
    this.tabMenu = [
      {
        label: 'Gyerekek',
        routerLink: ['children', this.selectedGroup.id]
      },
      {
        label: 'Óvónők',
        routerLink: ['teachers', this.selectedGroup.id]
      },
      {
        label: 'Felhívások',
        routerLink: ['alerts', this.selectedGroup.id]
      }
    ];
  }

  refreshGroupList(){
    this.displayNewGroup = false;
    this.service.getSimpleGroupList().subscribe(response => {
      this.groups = new Array<{name, id}>();
      for(let g of response.json()) {
        this.groups.push({name: g.name, id: g.id});
      }
      this.selectedGroup = this.groups[0];
      this.refreshTabMenu();
      this.router
        .navigate(['/main/admin/group-manager/children', this.selectedGroup.id])
        .catch( reason => {
          console.log(reason);
        });
    });
  }

}
