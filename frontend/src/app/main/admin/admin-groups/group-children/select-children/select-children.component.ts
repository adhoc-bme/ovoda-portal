import { Component, OnInit } from '@angular/core';
import {KindergartenService} from '../../../../../services/kindergarten.service';
import {EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-select-children',
  templateUrl: './select-children.component.html',
  styleUrls: ['./select-children.component.css']
})
export class SelectChildrenComponent implements OnInit {
  @Input()  group_id: number;
  @Output() finish = new EventEmitter<Array<{id: number, name: string, group_id: number, parent_id: number, sign: string}>>();

  children: Array<{id: number, name: string, group_id: number, parent_id: number, sign: string}>;
  selected_children: Array<string>;

  constructor(
    private service: KindergartenService,
  ) {
  }

  ngOnInit() {
    this.group_id = 0;
    this.children = [];
    this.selected_children = [];
    this.refreshChildren();
  }

  okOnClick() {
    let children_temp = new Array<{id: number, name: string, group_id: number, parent_id: number, sign: string}>();
    for(let c of this.children){
      if (this.selected_children.includes(c.id.toString())) {
        children_temp.push(c);
      }
    }
    this.finish.emit(children_temp);
    this.selected_children = [];
    this.refreshChildren();
  }

  refreshChildren(){
    this.service.getChildrenWithoutGroup().subscribe(response => {
      for(let c of response.json()){
        this.children.push({id: c.id, name: c.name, group_id: c.groupId, parent_id: c.parentId, sign: c.sign});
      }
    });
  }



}
