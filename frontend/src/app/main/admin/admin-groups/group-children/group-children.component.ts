import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {KindergartenService} from '../../../../services/kindergarten.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-group-children',
  templateUrl: './group-children.component.html',
  styleUrls: ['./group-children.component.css']
})
export class GroupChildrenComponent implements OnInit {

  group_id: number;
  children: any[];
  cols;
  displayChildrenEdit: boolean;
  displayChildEdit: boolean;
  selected_child_id: number;


  constructor(
    private service: KindergartenService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    console.log('main-admin-group-manager-children');
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshChildren();
    });
    this.cols = [
      {field: 'name', header: 'Név'},
      {field: 'parent', header: 'Szülő'},
      {field: 'sign', header: 'Jel'},
      {field: 'child_id', header: 'Child_id'},
    ];
    this.displayChildrenEdit = false;
  }

  refreshChildren() {
    if(this.group_id === -1 || this.group_id === undefined){
      this.children = new Array();
      console.log('Invalid group id: ' + this.group_id);
    } else {
      this.service.getAllChildren().
      subscribe(response => {
        this.children = new Array();
        for (let  child of response.json()){
          if(child.groupId === this.group_id){
            this.children.push({
              name: child.name,
              parent: '',
              sign: child.sign,
              child_id: child.id,
              parent_id: child.parentId
            });
          }
        }
        for(let c of this.children){
          this.service.getUserById(c.parent_id).
          subscribe(response2 => {
            c.parent = response2.json().name;
          });
        }
      });

    }

  }


  setDisplayChildrenEdit(){
    this.displayChildrenEdit = true;
  }

  onFinish(children: Array<{id: number, name: string, group_id: number, parent_id: number, sign: string}>){
    for(let c of children){
      this.service.updateChild(c.id, c.name, this.group_id, c.parent_id, c.sign).subscribe(response => {
      });
    }
  this.refreshChildren();
  this.displayChildrenEdit = false;
}

  removeChildFromGroup(child) {
    this.selected_child_id = child.child_id;
    this.service.updateChild(child.child_id, child.name, null, child.parent_id, child.sign).subscribe(response => {
    });
  }

  childEdited(id: number){
    this.displayChildEdit = false;
    /*TODO*/
  }
}
