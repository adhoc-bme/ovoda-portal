import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Message} from 'primeng/api';
import {KindergartenService} from '../../../../services/kindergarten.service';
import {formatDate} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-group-alerts',
  templateUrl: './group-alerts.component.html',
  styleUrls: ['./group-alerts.component.css']
})
export class GroupAlertsComponent implements OnInit{
  group_id: number;
  alerts: string[][];
  newText: string;
  msgs: Message[] = [];
  showNew: boolean;

  constructor(
    private service: KindergartenService,
    private route: ActivatedRoute
    ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshAlerts();
    });
    this.alerts = new Array();
    this.showNew = false;
    this.newText = '';
    this.refreshAlerts();
  }

  sendNewA() {
    if (this.newText !== '') {
      this.msgs = [];
      this.service.sendNewAlert(this.newText, formatDate(new Date(), 'yyyy/MM/dd-hh:mm', 'en'), this.group_id).subscribe(response => {
        this.refreshAlerts();
      });
    } else {
      this.msgs.push({severity: 'error', summary: 'Hiba!', detail: 'Töltsön ki minden mezőt!'});
    }
  }

  cancel() {
    this.showNew = false;
    this.newText = '';
  }

  show() {
    this.showNew = true;
  }

  deleteAlertById(id: number){
    this.service.deleteAlertById(id).subscribe(response => {
      this.refreshAlerts();
    });
  }

  refreshAlerts(){
    this.service.getGroupById(this.group_id).subscribe(response => {
      this.alerts = new Array();
      this.alerts = response.json().alerts;
    });
  }
}
