import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {KindergartenService} from '../../../../services/kindergarten.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.css']
})
export class UserEditorComponent implements OnInit {
  @Input() user: number;
  @Output() finish = new EventEmitter<number>();

  user_tmp: {
    id: number,
    name: string,
    email: string,
    phone: string;
    role: Role;
    address: {zip: number, street: string, town: string}};
  roles = [
    {name: 'ADMIN'},
    {name: 'TEACHER'},
    {name: 'PARENT'},
    {name: 'NOROLE'}
    ];
  current_role;

  loaded: {a: boolean, b: boolean, c: boolean};

  constructor(
    private service: KindergartenService
  ) { }

  ngOnInit() {
    this.loaded = {a: false, b: false, c: false};

    this.service.getUserById(this.user).subscribe(response => {
      let res = response.json();
      this.user_tmp = res;
      this.current_role = {name: res.role};
      this.loaded = {a: true, b: true, c: true};
    });


  }

  updateUser(){
      this.user_tmp.role = this.current_role.name;
      this.service.updateUser(this.user_tmp).subscribe(response => {
        this.finish.emit(this.user_tmp.id);
      });
  }

  deleteUser(event){
    this.service.deleteUserById(this.user_tmp.id).subscribe(response => {
      this.finish.emit(this.user_tmp.id);
    });
  }


  isLoaded(){
    return this.loaded.a && this.loaded.b && this.loaded.c;
  }

}

export enum Role {
  'ADMIN',
  'TEACHER',
  'PARENT',
  'NOROLE'
}
