import { Component, OnInit } from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-admin-teachers',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  display_editor: boolean;
  users: Array<{
    id: number,
    name: string,
    phone: string,
    email: string,
    address: string,
    role: string
  }>;
  cols;
  selected_user: {id: number, name: string, email: string, phone: string, role: string, address: {zip: number, street: string, town: string}};

  constructor(private service: KindergartenService) {

  }

  ngOnInit() {
    this.display_editor = false;
    this.users = [];
    this.service.getAllUsers().subscribe(response => {
      for(let u of response.json()){
        this.users.push({
          id: u.id,
          name: u.name,
          phone: u.phone,
          email: u.email,
          address: u.address.town + ' ' + u.address.street + ' ' + u.address.zip,
          role: u.role
        });
      }
    })

    this.cols = [
      { field: 'name', header: 'Név' },
      { field: 'role', header: 'Jog'},
      { field: 'phone', header: 'Telefon' },
      { field: 'email', header: 'E-mail' },
      { field: 'address', header: 'Cím' }
    ];
  }

  editUser(user) {
    this.selected_user = user;
    this.display_editor = true;
  }

  editFinish(event){
    this.display_editor = false;
  }

}
