import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-edit-child',
  templateUrl: './edit-child.component.html',
  styleUrls: ['./edit-child.component.css']
})
export class EditChildComponent implements OnInit, OnChanges {
  @Input() child_id: number;
  @Output() finished = new EventEmitter<number>();
  child;


  constructor(private service: KindergartenService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    this.child = {name: 'Voldemort', sign: 'Nazgul', parentId: '111111', groupId: '1201220'};
    this.service.getAllChildren().subscribe(response => {
      for(let c of response.json()){
        if(c.id === this.child_id){
          this.child = c;
        }
      }
    });
  }

  finish(){
    this.finished.emit(this.child_id);
  }

}
