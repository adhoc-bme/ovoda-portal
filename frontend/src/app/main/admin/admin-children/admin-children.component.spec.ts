import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminChildrenComponent } from './admin-children.component';

describe('AdminChildrenComponent', () => {
  let component: AdminChildrenComponent;
  let fixture: ComponentFixture<AdminChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
