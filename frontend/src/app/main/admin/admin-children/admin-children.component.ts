import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-admin-children',
  templateUrl: './admin-children.component.html',
  styleUrls: ['./admin-children.component.css']
})
export class AdminChildrenComponent implements OnInit {

  group_filter: SelectItem[];
  parent_filter: Array<{label: string, value: number, filter: string}>;
  cols: any;
  selected_child;
  display_editor;
  display_creator;
  children: Array<{
    id: number,
    name: string,
    sign: string,
    parent_id: number,
    parent: string,
    group_id: number,
    group: string}>

  constructor(private service: KindergartenService) { }

  ngOnInit() {
    this.display_editor = false;
    this.display_creator = false;
    this.group_filter = [
      {label: 'Összes', value: null},
      {label: 'Csoportban', value: 'Grouped'},
      {label: 'Nincs csoportban', value: 'NonGrouped'}
    ];

    this.parent_filter = [
      {label: 'Összes', value: null, filter: 'equals'},
      {label: 'Van szülő', value: 0, filter: 'gte'},
      {label: 'Nincs szülő', value: 0, filter: 'lt'}
    ];

    this.cols = [
    { field: 'name', header: 'Név' },
    { field: 'sign', header: 'Jel' },
    { field: 'parent', header: 'Szülő' },
    { field: 'group', header: 'Group' }
  ];
    this.refreshChildren();

  }

  newChild(){
    this.display_creator = true;
  }

  editChild(child){
      this.selected_child = child;
      this.display_editor = true;
  }

  parentFilter(dt){
  }

  editFinish(event){
    this.display_editor = false;
    this.refreshChildren();
  }

  refreshChildren(){
    this.children = [];
    this.service.getAllChildren().subscribe(response => {
      for(let c of response.json()){
        this.children.push({
          id: c.id,
          name: c.name,
          sign: c.sign,
          parent_id: c.parentId,
          parent: c.parentId,
          group_id: c.groupId,
          group: c.groupId});
      }
    });
  }

}
