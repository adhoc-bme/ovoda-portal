import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {KindergartenService} from '../../../../services/kindergarten.service';

@Component({
  selector: 'app-new-child',
  templateUrl: './new-child.component.html',
  styleUrls: ['./new-child.component.css']
})
export class NewChildComponent implements OnInit {
  @Output() finish = new EventEmitter<number>();

  child: {id: number, name: string, group_id: number, parent_id: number, sign: string};
  simple_groups: Array<{id: number, name: string}>;
  selected_group: {id: number, name: string};
  simple_parents: Array<{id: number, name: string}>;
  selected_parent: {id: number, name: string};
  loaded: {a: boolean, b: boolean, c: boolean};

  constructor(
    private service: KindergartenService
  ) { }

  ngOnInit() {

    this.loaded = {a: false, b: false, c: false};
    this.child = {id: null, name: null, group_id: null, parent_id: null, sign: null};
    this.refreshSimpleArrays();
  }

  createChild(){
    this.service.createChildid(this.child.id, this.child.name, this.selected_group.id, this.selected_parent.id, this.child.sign)
      .subscribe(response => {
        this.finish.emit(this.child.id);
      });
  }

  refreshSimpleArrays(){
    this.simple_groups = [];
    this.simple_parents = [];
    this.service.getSimpleGroupList().subscribe(response => {
      this.simple_groups.push({id: null, name: 'Nincs Csoport'});
      for(let g of response.json()){
        this.simple_groups.push({id: g.id, name: g.name});
      }
      this.selected_group = {id: null, name: 'Nincs Csoport'};
      this.loaded.a = true;
    });
    this.simple_parents.push({id: null, name: 'Nincs megadva'});
    this.selected_parent = {id: null, name: 'Nincs megadva'};
    this.loaded.b = true;

    this.service.getPotentialParents().subscribe(response => {
      for(let p of response.json()){
        this.simple_parents.push({id: p.id, name: p.name});
      }
      this.loaded.c = true;
    });
  }

  isLoaded(){
    return this.loaded.a && this.loaded.b && this.loaded.c;
  }

}
