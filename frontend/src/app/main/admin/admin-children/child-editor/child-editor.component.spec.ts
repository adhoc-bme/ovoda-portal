import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildEditorComponent } from './child-editor.component';

describe('ChildEditorComponent', () => {
  let component: ChildEditorComponent;
  let fixture: ComponentFixture<ChildEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
