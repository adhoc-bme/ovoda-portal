import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {KindergartenService} from '../../../../services/kindergarten.service';

@Component({
  selector: 'app-child-editor',
  templateUrl: './child-editor.component.html',
  styleUrls: ['./child-editor.component.css']
})
export class ChildEditorComponent implements OnInit {
  @Input() child: {id: number, name: string, group_id: number, parent_id: number, sign: string};
  @Output() finish = new EventEmitter<number>();

  child_tmp: {id: number, name: string, group_id: number, parent_id: number, sign: string};
  simple_groups: Array<{id: number, name: string}>;
  simple_parents: Array<{id: number, name: string}>;
  selected_parent: {id: number, name: string};
  selected_group: {id: number, name: string};
  loaded: {a: boolean, b: boolean, c: boolean};

  constructor(
    private service: KindergartenService
  ) { }

  ngOnInit() {

    this.loaded = {a: false, b: false, c: false};
    this.child_tmp = {id: this.child.id, name: this.child.name, group_id: this.child.group_id, parent_id: this.child.parent_id, sign: this.child.sign};
    this.refreshSimpleArrays();
  }

  updateChild(){
    this.service.updateChild(this.child_tmp.id, this.child_tmp.name, this.selected_group.id, this.selected_parent.id, this.child_tmp.sign)
      .subscribe(response => {
        this.finish.emit(this.child_tmp.id);
      });
  }

  deleteChild(event){
    this.service.deleteChildById(this.child_tmp.id).subscribe(response => {
      this.finish.emit(this.child_tmp.id);
    });
  }

  refreshSimpleArrays(){
    this.simple_groups = [];
    this.simple_parents = [];
    this.service.getSimpleGroupList().subscribe(response => {
      this.simple_groups.push({id: null, name: 'Nincs Csoport'});
      for(let g of response.json()){
        this.simple_groups.push({id: g.id, name: g.name});
        if(this.child_tmp.group_id === g.id){
          this.selected_group = {id: g.id, name: g.name};
        }
      }
      this.loaded.a = true;
    });
    this.simple_parents.push({id: null, name: 'Nincs megadva'});
    if (this.child_tmp.parent_id !== null){
      this.service.getUserById(this.child_tmp.parent_id).subscribe(response => {
        this.simple_parents.push({id: response.json().id, name: response.json().name});
        this.selected_parent = {id: response.json().id, name: response.json().name};
        this.loaded.b = true;
      });
    } else {
      this.selected_parent = {id: null, name: 'Nincs megadva'};
      this.loaded.b = true;
    }

    this.service.getPotentialParents().subscribe(response => {
      for(let p of response.json()){
        this.simple_parents.push({id: p.id, name: p.name});
      }
      this.loaded.c = true;
    });
  }

  isLoaded(){
    return this.loaded.a && this.loaded.b && this.loaded.c;
  }

}
