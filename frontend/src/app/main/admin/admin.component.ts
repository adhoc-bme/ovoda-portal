import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  tabMenu: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.tabMenu = [
      {
        label: 'Csoportok kezelése',
        routerLink: ['group-manager']
      },
      {
        label: 'Gyerekek kezelése',
        routerLink: ['child-manager']
      },
      {
        label: 'Felhasználók kezelése',
        routerLink: ['user-manager']
      }
    ];
  }
}


