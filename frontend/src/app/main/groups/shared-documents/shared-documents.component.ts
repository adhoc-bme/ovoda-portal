import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';

@Component({
  selector: 'app-shared-documents',
  templateUrl: './shared-documents.component.html',
  styleUrls: ['./shared-documents.component.css']
})
export class SharedDocumentsComponent implements OnInit, OnChanges {
  @Input() group_id: number;

  constructor(private service: KindergartenService) { }

  ngOnInit() {
  }

  goDrive() {
    /*TODO*/
    console.log('Group id: ' + this.group_id + ' GoogleDrive');
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}): void {
    this.group_id = changes.group_id.currentValue.id;
  }

}
