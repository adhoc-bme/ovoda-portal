import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Message} from 'primeng/api';
import {KindergartenService} from '../../../services/kindergarten.service';
import {formatDate} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-announcements',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit, OnChanges {
  @Input() group_id: number;


  alerts: string[][];
  newText: string;
  msgs: Message[] = [];
  showNew: boolean;
  role: number;

  constructor(
    private service: KindergartenService,
    private user_service: UserService,
    private route: ActivatedRoute
    ) {}

  ngOnInit() {
    this.role = this.user_service.getRole();
    this.alerts = new Array();
    this.showNew = false;
    this.newText = '';
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshAlerts();
    });

  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}){
    this.group_id = changes.group_id.currentValue.id;
    this.refreshAlerts();
    /*TODO*/
  }

  sendNewA() {
    if (this.newText !== '') {
      this.msgs = [];
      this.service.sendNewAlert(this.newText, formatDate(new Date(), 'yyyy/MM/dd-hh:mm', 'en'), this.group_id).subscribe(response => {
        this.refreshAlerts();
      });
    } else {
      this.msgs.push({severity: 'error', summary: 'Hiba!', detail: 'Töltsön ki minden mezőt!'});
    }
    this.showNew = false;
  }

  cancel() {
    this.showNew = false;
    this.newText = '';
  }

  show() {
    this.showNew = true;
  }

  refreshAlerts(){
    this.service.getGroupById(this.group_id).subscribe(response => {
      this.alerts = new Array();
      this.alerts = response.json().alerts;
    });
  }
}
