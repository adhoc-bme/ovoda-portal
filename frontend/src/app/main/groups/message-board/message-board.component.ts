import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';
import {formatDate} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../services/user.service';


@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.css'],
})
export class MessageBoardComponent implements OnInit{
  group_id: number;
  role: number;

  messages: Array<{id: number, message: string, poster_id: number, date: string, has_image: boolean, poster: string}>;
  displayNew = false;
  newPostText = '';
  constructor(
    private service: KindergartenService,
    private user_service: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.role = this.user_service.getRole();
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshPosts();
    });
  }

  displayNewMessage() {
    this.displayNew = true;
  }

  cancelNewM() {
    this.newPostText = '';
    this.displayNew = false;
  }

  sendNewM() {
    this.service.createPost(this.newPostText, +localStorage.getItem('userId'), formatDate(new Date(), 'yyyy/MM/dd-hh:mm', 'en'), false, this.group_id).subscribe(response => {
      this.refreshPosts();
      this.displayNew = false;
    });
  }

  refreshPosts(){
    this.service.getPostsByGroupId(this.group_id).subscribe(response => {
      this.messages = [];
      let msg_temp = [];
      for(let p of response.json()){
        this.service.getUserById(p.posterId).subscribe(response2 => {
          msg_temp.unshift({id: p.id, message: p.message, poster_id: p.posterId, date: p.date, has_image: p.hasImage, poster: response2.json().name});
        });
      }
      this.messages = msg_temp;
    });
  }

}


