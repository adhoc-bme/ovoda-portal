import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {KindergartenService} from '../../../services/kindergarten.service';
import {ActivatedRoute} from '@angular/router';



@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  group_id: number;
  children: any[];
  cols;


  constructor(
    private service: KindergartenService,
    private route: ActivatedRoute
    ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.group_id = +params.get('groupId');
      this.refreshChildren();
    });
    this.cols = [
      {field: 'name', header: 'Név'},
      {field: 'parent', header: 'Szülő'},
      {field: 'sign', header: 'Jel'}
    ];
  }

  refreshChildren() {
    if(this.group_id === -1 || this.group_id === undefined){
      this.children = new Array();
      console.log('Invalid group id: ' + this.group_id);
    } else {

      this.service.getAllChildren().
      subscribe(response => {
        this.children = new Array();
        for (let  child of response.json()){
          if(child.groupId === this.group_id){
            this.children.push({
              name: child.name,
              parent: '',
              sign: child.sign,
              parent_id: child.parentId
            });
          }
        }
        for(let c of this.children){
          this.service.getUserById(c.parent_id).
          subscribe(response2 => {
            c.parent = response2.json().name;
          });
        }
      });
    }
  }
}
