import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {Router} from '@angular/router';
import {KindergartenService} from '../../services/kindergarten.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  tabMenu: MenuItem[];
  groups_name;
  selectedGroup: {name: string, id: number};
  user_id: number;


  constructor(
    private service: KindergartenService,
    private user_service: UserService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.user_id = this.user_service.getUserId();
    console.log('main-groups-OnInit');
    this.selectedGroup = {name: '', id: -1};
    this.refreshGroupList();
  }


  onClickTM() {
    this.refreshTabMenu();
    this.router.navigate(['/main/groups/members', this.selectedGroup.id]).catch( reason => {
    });
  }

refreshGroupList(){
    this.service.getRelevantGroupsById(this.user_id).subscribe(response => {
      this.groups_name = new Array<{name, id}>();
      for(let g of response.json()) {
        this.groups_name.push({name: g.name, id: g.id});
      }
      this.selectedGroup = this.groups_name[0];
      this.refreshTabMenu();
    });
}

refreshTabMenu(){
  this.tabMenu = [
    {
      label: 'Gyerekek',
      routerLink: ['members', this.selectedGroup.id]/*,
      command: (event) => this.onClickTM()*/
    },
    {
      label: 'Üzenőfal',
      routerLink: ['message-board', this.selectedGroup.id]
    },
    {
      label: 'Felhívások',
      routerLink: ['alerts', this.selectedGroup.id]
    },
    {
      label: 'Megosztott anyagok',
      routerLink: ['documents', this.selectedGroup.id]
    },
  ];
}


}
