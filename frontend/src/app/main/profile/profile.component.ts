import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {KindergartenService} from '../../services/kindergarten.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user_id: number;
  current;
  pass0;
  pass1;
  pass2;
  load: boolean;

  constructor(
    private user_service: UserService,
    private service: KindergartenService
    ) {
  }

  ngOnInit() {
    this.load = false;
    this.user_id = this.user_service.getUserId();
    this.service.getUserById(this.user_id).subscribe(response => {
      let u = response.json();
      this.current = u;
      this.load = true;
    });
  }

  saveChanges() {
    this.service.updateUser(this.current).subscribe(response => {
    });
  }

  setPass() {
    if(this.pass0 === this.current.pass && this.pass1 === this.pass2){
      this.current.pass = this.pass1;
      this.service.updateUser(this.current).subscribe(response => {
      });
    }
  }
}
